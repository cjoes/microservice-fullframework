﻿using ContactManager.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManager.Tests
{
    public class TestHelper
    {
        public List<ContactData> GetContactTestData()
        {
            List<ContactData> response = new List<ContactData>()
            {
                new ContactData(Guid.NewGuid().ToString(),"Bill","West","1122 Swan Rd.","Aurora","Colorado","80014","stuff","stuff1","stuff2"),
                new ContactData(Guid.NewGuid().ToString(),"Sam","West","7789 Duck Rd.","Aurora","Colorado","80012","stuff","stuff1","stuff2"),
                new ContactData(Guid.NewGuid().ToString(),"Fred","West","9895 Goose Rd.","Aurora","Colorado","80013","stuff","stuff1","stuff2")
            };

            return response;
        }

        public ContactData GetContactTestDataRecord(string contactId)
        {
            return new ContactData(contactId, "Bill", "West", "1122 Swan Rd.", "Aurora", "Colorado", "80014", "stuff", "stuff1", "stuff2");
        }

        public Contact GetContactTestRecord(string contactId)
        {
            return new Contact(contactId, "Jimmy", "Johnson", "5567 Goose Ln.", "Aurora", "Colorado", "80012");
        }
    }
}
