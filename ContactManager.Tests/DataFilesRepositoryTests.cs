﻿using ContactManager.Service.Repositories;
using NUnit.Framework;
using System.Linq;

namespace ContactManager.Tests
{
    [TestFixture]
    public class DataFilesRepositoryTests
    {
        private readonly IRepository dataFilesRepo = null;
        private readonly TestHelper testHelper = null;

        public DataFilesRepositoryTests()
        {
            this.dataFilesRepo = new DataFiles();
            this.testHelper = new TestHelper();
        }
        [Test]
        public void TestGetContactData()
        {
            
            var contactData = this.dataFilesRepo.GetContactData();
            Assert.IsNotNull(contactData);
            Assert.AreEqual(Enumerable.Count(contactData), 5);
        }

        [Test]
        public void TestUpdateContact()
        {
            //var existingContactData = this.dataFilesRepo.GetContactData();
            var newContactData = testHelper.GetContactTestDataRecord("f35e46b0-d93d-4c4c-9f0f-2a91805e5c9c");

            dataFilesRepo.UpdateContact(newContactData);

            var contactDataAfterUpdate = this.dataFilesRepo.GetContactData();

            Assert.NotNull(contactDataAfterUpdate);

            var contactUpdated = contactDataAfterUpdate.Any(c => c.ContactId == newContactData.ContactId &&
                                                                 c.FirstName == newContactData.FirstName &&
                                                                 c.LastName == newContactData.LastName);

            Assert.NotNull(contactUpdated);


        }
    }
}
