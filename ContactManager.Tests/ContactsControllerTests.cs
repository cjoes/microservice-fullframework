﻿using NUnit.Framework;
using Moq;
using ContactManager.Service.Repositories;
using ContactManager.Service.Models;
using System.Collections.Generic;
using ContactManager.Service.Controllers;

namespace ContactManager.Tests
{
    [TestFixture]
    class ContactsControllerTests
    {
        private Mock<IRepository> dataFileRepo;
        private readonly TestHelper helper = null;

        public ContactsControllerTests()
        {
            this.helper = new TestHelper();
        }

        [Test]
        public void TestGetContatcsOperation()
        {
            var testData = helper.GetContactTestData();
            dataFileRepo = new Mock<IRepository>();
            dataFileRepo.Setup(drepo => drepo.GetContactData())
                        .Returns(testData);

            IContactsController controller = new ContactsController(dataFileRepo.Object);

            var result = controller.Get();

            int index = 0;
            Assert.IsNotNull(result);

            foreach(Contact contact in result)
            {
                Assert.AreEqual(contact.Address, testData[index].Address);
                Assert.AreEqual(contact.City, testData[index].City);
                Assert.AreEqual(contact.ContactId, testData[index].ContactId);
                Assert.AreEqual(contact.FirstName, testData[index].FirstName);
                Assert.AreEqual(contact.LastName, testData[index].LastName);
                Assert.AreEqual(contact.State, testData[index].State);
                Assert.AreEqual(contact.Zip, testData[index].Zip);
                index++;
            }
        }

        [Test]
        public void TestPutContactOperation()
        {
            dataFileRepo = new Mock<IRepository>();
            dataFileRepo.Setup(drepo => drepo.UpdateContact(It.IsAny<ContactData>()));

            IContactsController controller = new ContactsController(dataFileRepo.Object);
            var testRecord = helper.GetContactTestRecord("680e6956-0bd4-4ed6-9f17-c745efc6edd1");

            controller.Put(testRecord);
            
        }

    }

}