﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManager.Service
{
    class Program
    {
        static void Main()
        {
            var baseAddress = "http://localhost/8080/";

            //Start OWIN Host

            using(WebApp.Start<Startup>(url:baseAddress))
            {
                Console.WriteLine($"Service Listening at {baseAddress}");

                System.Threading.Thread.Sleep(-1);
            }
        }
    }
}
