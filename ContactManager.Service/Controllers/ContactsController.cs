﻿using ContactManager.Service.Models;
using ContactManager.Service.Repositories;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace ContactManager.Service.Controllers
{
    public class ContactsController : ApiController, IContactsController
    {
        private readonly IRepository contactDataStore;

        public ContactsController(IRepository contactsDataStore)
        {
            this.contactDataStore = contactsDataStore;
        }

        public ContactsController() : this(new DataFiles())
        { }

        /// <summary>
        /// Route: http://localhost/8080/api/contacts as GET
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Contact> Get()
        {
            Console.WriteLine("Getting Contacts");
            List<Contact> contacts = new List<Contact>();

            foreach(ContactData contactData in contactDataStore.GetContactData())
            {
                var contact = new Contact(
                    ContactId: contactData.ContactId,
                    firstName: contactData.FirstName,
                    lastName: contactData.LastName,
                    address: contactData.Address,
                    city: contactData.City,
                    state: contactData.State,
                    zip: contactData.Zip);

                contacts.Add(contact);
            }

            return contacts;
        }

        /// <summary>
        /// http://localhost/8080/api/contacts as a POST
        /// </summary>
        /// <param name="contact"></param>
        public void Put(Contact contact)
        {
            Console.WriteLine("Updateing Contact");
            var contactData = new ContactData(
                contactId: contact.ContactId,
                firstName: contact.FirstName,
                lastName: contact.LastName,
                address: contact.Address,
                city: contact.City,
                state: contact.State,
                zip: contact.Zip,
                internalData1: "",
                internalData2: "",
                internalData3: "");

            contactDataStore.UpdateContact(contactData);
        }
    }
}
