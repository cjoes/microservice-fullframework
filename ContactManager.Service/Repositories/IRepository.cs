﻿using ContactManager.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManager.Service.Repositories
{
    public interface IRepository
    {
        IEnumerable<ContactData> GetContactData();
        void UpdateContact(ContactData contactData);
    }
}
